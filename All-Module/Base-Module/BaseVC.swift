//
//  BaseVC.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import UIKit

class BaseVC: UIViewController {

    //MARK: - PROPERTIES
    
    //MARK: - LOAD ALL VIEWS IN MEMORY 
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - MOVE TO PREVIOUS SCREEN
    public func POP(_ animated : Bool = true){
        self.navigationController?.popViewController(animated: animated)
    }
    
    //MARK: - DISMISS THE CURRENT SCREEN
    public func DISMISS(_ animated : Bool = true){
        self.dismiss(animated: animated)
    }
    
    //MARK: - MOVE TO USERS LIST SCREEN
    public func MoveToUserListScreen(){
        let vc = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: IdentifireName.kUserVC) as! UserVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - MOVE TO MAP SCEEN
    public func MoveToMapScreen(user:UserCodable){
        let vc = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil).instantiateViewController(withIdentifier: IdentifireName.kMapVC) as! MapVC
        vc.viewModel.model.user = user
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - SHOW THE ALERT
    public func ShowAlert(_ title : String = .kEmpty, _ msg : String = .kEmpty , _ isTwoButton : Bool = false,completion: @escaping () -> ()){
        let alert = UIAlertController(title:title , message: msg, preferredStyle: .alert)
        
        if isTwoButton {
            alert.addAction(UIAlertAction(title: .kCancel, style: .default, handler: {
                action in
            }))
            alert.addAction(UIAlertAction(title: .kYes, style: .default, handler: {
                action in
                completion()
            }))
        }else{
            alert.addAction(UIAlertAction(title: .kOK, style: .default, handler: {
                action in
                completion()
            }))
        }
        self.present(alert, animated: true, completion: nil)
    }
}
