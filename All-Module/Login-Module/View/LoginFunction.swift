//
//  LoginFunction.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation
import UIKit
import ADCountryPicker


extension ViewController {
    
    //MARK: - SIGN IN ACTION
    @IBAction func signInAction(){
        self.signInButton.Animation()
        self.signInTitleLabel.Animation()
        self.viewModel.isValidation(user: self.userTextField.text!.trim(), pass: self.passTextField.text!.trim(), country: self.countyTextField.text!.trim(), completion: { isValid, msg in
            self.ShowAlert(.kCredential, msg!) {
                if isValid{
                    UserDefaults.standard.setValue(true, forKey: .kLOGIN_KEY)
                    self.MoveToUserListScreen()
                }
            }
        })
    }
    
    //MARK: - EYE ACTION
    @IBAction func eyeActionAction(){
        self.eyeImageView.Animation()
        self.isEyeOpen.toggle()
        self.eyeImageView.image = UIImage(named: self.isEyeOpen ? ImageName.eyeOpen.rawValue : ImageName.eyeClose.rawValue)
        self.passTextField.isSecureTextEntry = self.isEyeOpen ? false : true
    }
    
    //MARK: - COUNTY ACTION
    @IBAction func countyAction(){
        let picker = ADCountryPicker(style: .grouped)
        picker.didSelectCountryClosure = { name, code in
            self.countyTextField.text = name
            self.DISMISS()
        }
        let nav = UINavigationController(rootViewController: picker)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
    
    //MARK: - AMIMATION THE CONTROL
    func animateTheControl(){
        self.signInLabel.Animation()
        self.pleaseSignInLabel.Animation()
        self.userNameImageView.Animation()
        self.passwordImageView.Animation()
        self.countyImageView.Animation()
        self.userTextField.Animation()
        self.passTextField.Animation()
        self.countyTextField.Animation()
        self.signInTitleLabel.Animation()
        self.eyeImageView.Animation()
        self.logoImageView.Animation()
    }
}
