//
//  LoginSqlite.swift
//  CartrackDemo
//
//  Created by Shailendra on 29/12/22.
//

import Foundation
import SQLite

class DBHelper : DBDelegage{
  
    //MARK: - PROPERTIES
    static let shared = DBHelper()
    private init(){}
    
    
    //MARK: - INSERT VALUE IN DATABASE
    func insertValueInSQLite(){
        do {
            let db = try Connection(self.getPath())
            let users = Table(.kUsers)
            let id = Expression<Int64>(.kId)
            let name = Expression<String>(.kName)
            let password = Expression<String>(.kPassword)
            
            try db.run(users.create { t in
                t.column(id, primaryKey: true)
                t.column(name)
                t.column(password, unique: true)
            })
            let insert = users.insert(name <- .kCartrack, password <- .kCartrack)
            try db.run(insert)
        }catch {
            print(error)
        }
    }
    
    //MARK: - GET USERS LIST
    func getUsersList()->Int{
        do {
            let db = try Connection(getPath())
            let users = Table(.kUsers)
            return try db.scalar(users.count)
        }catch {
            print(error)
        }
        return 0
    }
    
    //MARK: - GET USER NAME AND PASSWORD
    func getUsersNameAndPassword()->(String,String){
        do {
            var userNameStr : String = .kEmpty
            var passwordStr : String = .kEmpty
            let db = try Connection(getPath())
            let users = Table(.kUsers)
            let name = Expression<String>(.kName)
            let password = Expression<String>(.kPassword)
            for user in try db.prepare(users) {
                userNameStr = user[name]
                passwordStr = user[password]
            }
            return (userNameStr,passwordStr)
        }catch {
            print(error)
        }
        return (.kEmpty,.kEmpty)
    }
    
    //MARK: - GET LOCAL SQLITE PATH
    func getPath()->String{
        let dbPath: String = .kSqlite
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent(dbPath)
        return fileURL.path
    }
}
