//
//  LoginTextField.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation
import UIKit

//============================================
//MARK: - UITextFieldDelegate
//============================================
extension ViewController : UITextFieldDelegate {
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
