//
//  ViewController.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import UIKit
import ADCountryPicker

class ViewController: BaseVC {

    //MARK: - PROPERTIES
    @IBOutlet weak var userTextField     : UITextField!
    @IBOutlet weak var passTextField     : UITextField!
    @IBOutlet weak var countyTextField   : UITextField!
    @IBOutlet weak var eyeImageView      : UIImageView!
    @IBOutlet weak var userNameImageView : UIImageView!
    @IBOutlet weak var passwordImageView : UIImageView!
    @IBOutlet weak var countyImageView   : UIImageView!
    @IBOutlet weak var logoImageView     : UIImageView!
    @IBOutlet weak var signInLabel       : UILabel!
    @IBOutlet weak var signInTitleLabel  : UILabel!
    @IBOutlet weak var pleaseSignInLabel : UILabel!
    @IBOutlet weak var signInButton      : UIButton!
    var isEyeOpen                       = false
    lazy var viewModel = {
        LoginVM()
    }()
    
    //MARK: - LOAD ALL VIEWS IN MEMORY 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.animateTheControl()
        if DBHelper.shared.getUsersList() == 0 {
            DBHelper.shared.insertValueInSQLite()
        }
    }
}

