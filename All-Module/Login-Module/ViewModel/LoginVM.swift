//
//  LoginVm.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation
import SQLite

class LoginVM {
    
    func isValidation(user userName : String, pass password : String, country : String,  completion: @escaping (Bool, String?) -> ()) {
        
        if userName == .kEmpty && password == .kEmpty && country == .kEmpty {
            completion(false, Message.EmptyUserNamePasswordCounty())
        }else if userName == .kEmpty {
            completion(false, Message.EmptyUserName())
        }else if password == .kEmpty {
            completion(false, Message.EmptyPassword())
        }else if country == .kEmpty {
            completion(false, Message.EmptyCountry())
        }else {
            if userName.lowercased() == DBHelper.shared.getUsersNameAndPassword().0.lowercased() && password.lowercased() == DBHelper.shared.getUsersNameAndPassword().1{
                completion(true, Message.LoginSuccssfully())
            }else{
                completion(false, Message.NotMatchUserNamePassword())
            }
        }
        completion(true, Message.LoginSuccssfully())
    }
}
