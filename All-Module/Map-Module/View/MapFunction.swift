//
//  MapMapFunction.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation

extension MapVC {
    
    //MARK: - MOVE TO PRVIOUSE SCREEN
    @IBAction private func backAction(){
        self.POP()
    }
    
    //MARK: - AMIMATION THE CONTROL
    func animateTheControl(){
        self.headerLabel.Animation()
        self.backImageView.Animation()
    }
}

