//
//  MapMapView.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation
import MapKit

extension MapVC {
    
    //MARK: - SHOW THE MAP VIEW AND SET THE LAT LONG
    public func showTheMapView(){
        self.mapView.mapType = MKMapType.hybrid
       let location = CLLocationCoordinate2D(latitude:Double(self.viewModel.model.user?.address?.geo?.lat ?? "0.0") ?? 0.0 ,longitude: Double(self.viewModel.model.user?.address?.geo?.lng ?? "0.0") ?? 0.0)
        self.mapView.setRegion(MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)), animated: true)
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = self.getFullAddress()
        self.mapView.addAnnotation(annotation)
    }
    
    //MARK: - GET FULL ADDRESS
    func getFullAddress()-> String{
        let fullAddress = "\(self.viewModel.model.user?.address?.street ?? .kEmpty), \(self.viewModel.model.user?.address?.suite ?? .kEmpty), \(self.viewModel.model.user?.address?.city ?? .kEmpty), \(self.viewModel.model.user?.address?.zipcode ?? .kEmpty)"
        return fullAddress
    }
}
