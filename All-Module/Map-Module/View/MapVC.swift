//
//  MapVC.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import UIKit
import MapKit

class MapVC: BaseVC {

    //MARK: - PROPERTIES
    @IBOutlet weak var mapView       : MKMapView!
    @IBOutlet weak var headerLabel   : UILabel!
    @IBOutlet weak var backImageView : UIImageView!
    let annotation                   = MKPointAnnotation()
    var viewModel = {
        MapVM()
    }()
    
    //MARK: - LOAD ALL VIEWS IN MEMORY 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.animateTheControl()
        self.showTheMapView()
    }
}
