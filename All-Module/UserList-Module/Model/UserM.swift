//
//  UserM.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation

struct UserCodable: Codable {
    let id       : Int?
    let name     : String?
    let username : String?
    let email    : String?
    let address  : Address?
    let phone    : String?
    let website  : String?
    let company  : Company?
    
    enum CodingKeys: String, CodingKey {
        
        case id       = "id"
        case name     = "name"
        case username = "username"
        case email    = "email"
        case address  = "address"
        case phone    = "phone"
        case website  = "website"
        case company  = "company"
    }
    
    init(from decoder: Decoder) throws {
        let values    = try decoder.container(keyedBy: CodingKeys.self)
        self.id       = try values.decodeIfPresent(Int.self, forKey: .id)
        self.name     = try values.decodeIfPresent(String.self, forKey: .name)
        self.username = try values.decodeIfPresent(String.self, forKey: .username)
        self.email    = try values.decodeIfPresent(String.self, forKey: .email)
        self.address  = try values.decodeIfPresent(Address.self, forKey: .address)
        self.phone    = try values.decodeIfPresent(String.self, forKey: .phone)
        self.website  = try values.decodeIfPresent(String.self, forKey: .website)
        self.company  = try values.decodeIfPresent(Company.self, forKey: .company)
    }
    
}
struct Geo : Codable {
    let lat : String?
    let lng : String?
    
    enum CodingKeys: String, CodingKey {
        
        case lat = "lat"
        case lng = "lng"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.lat = try values.decodeIfPresent(String.self, forKey: .lat)
        self.lng = try values.decodeIfPresent(String.self, forKey: .lng)
    }
    
}

struct Company : Codable {
    let name        : String?
    let catchPhrase : String?
    let bs          : String?
    
    enum CodingKeys: String, CodingKey {
        
        case name        = "name"
        case catchPhrase = "catchPhrase"
        case bs          = "bs"
    }
    
    init(from decoder: Decoder) throws {
        let values       = try decoder.container(keyedBy: CodingKeys.self)
        self.name        = try values.decodeIfPresent(String.self, forKey: .name)
        self.catchPhrase = try values.decodeIfPresent(String.self, forKey: .catchPhrase)
        self.bs          = try values.decodeIfPresent(String.self, forKey: .bs)
    }
    
}

struct Address : Codable {
    let street  : String?
    let suite   : String?
    let city    : String?
    let zipcode : String?
    let geo     : Geo?
    
    enum CodingKeys: String, CodingKey {
        
        case street  = "street"
        case suite   = "suite"
        case city    = "city"
        case zipcode = "zipcode"
        case geo     = "geo"
    }
    
    init(from decoder: Decoder) throws {
        let values   = try decoder.container(keyedBy: CodingKeys.self)
        self.street  = try values.decodeIfPresent(String.self, forKey: .street)
        self.suite   = try values.decodeIfPresent(String.self, forKey: .suite)
        self.city    = try values.decodeIfPresent(String.self, forKey: .city)
        self.zipcode = try values.decodeIfPresent(String.self, forKey: .zipcode)
        self.geo     = try values.decodeIfPresent(Geo.self, forKey: .geo)
    }
}
