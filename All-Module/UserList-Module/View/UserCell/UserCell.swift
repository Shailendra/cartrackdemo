//
//  UserCell.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import UIKit

class UserCell: UITableViewCell {

    //MARK: - PROPETIES
    @IBOutlet weak var shortNameLabel   : UILabel!
    @IBOutlet weak var nameLabel        : UILabel!
    @IBOutlet weak var emailLabel       : UILabel!
    @IBOutlet weak var phoneLabel       : UILabel!
    @IBOutlet weak var websiteLabel     : UILabel!
    @IBOutlet weak var addressLabel     : UILabel!
    @IBOutlet weak var infoButton       : UIButton!
    @IBOutlet weak var shortbgImageView : UIImageView!
    @IBOutlet weak var rightImageView   : UIImageView!
    @IBOutlet weak var infoImageView    : UIImageView!
    
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - CONFIGURE THE TABLEVIEW CELL
    func configureCell(user: UserCodable){
        self.shortNameLabel.text = "\("\(user.name ?? .kEmpty)".first!)"
        self.nameLabel.text     = "\(user.name ?? .kEmpty), \(user.username ?? .kEmpty)"
        self.emailLabel.text    = user.email    ?? .kEmpty
        self.phoneLabel.text    = user.phone    ?? .kEmpty
        self.websiteLabel.text  = user.website  ?? .kEmpty
        self.addressLabel.text  = "\(user.address?.street ?? .kEmpty) \(user.address?.suite ?? .kEmpty) \(user.address?.city ?? .kEmpty) \(user.address?.zipcode ?? .kEmpty)"
        self.AnimationControl()
    }
    
    //MARK: - SCALE AMIMATION OF CONTROL
    private func AnimationControl(){
        self.shortNameLabel.Animation()
        self.nameLabel.Animation()
        self.emailLabel.Animation()
        self.phoneLabel.Animation()
        self.websiteLabel.Animation()
        self.addressLabel.Animation()
        self.infoButton.Animation()
        self.shortbgImageView.Animation()
        self.rightImageView.Animation()
        self.infoImageView.Animation()
        self.shortbgImageView.backgroundColor = .randomColor()
    }
}
