//
//  UserFunction.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation
import UIKit

extension UserVC {
    
    //MARK: - HIDE THE POPUP OF COMPANY
    @IBAction private func hidePopUp(){
        self.outerView.isHidden = true
    }
    
    //MARK: - AMIMATION THE CONTROL
    func animateTheControl(){
        self.headerLabel.Animation()
        self.logoutImageView.Animation()
    }
    
    //MARK: - SHOW THE POPUP OF COMPANY
    @objc func showCompanyView(sender:UIButton){
        self.getFullAddress(sender.tag)
    }
    
    //MARK: -  GET FULL ADDRESS
    private func getFullAddress(_ index : Int){
        self.outerView.isHidden = false
        self.addressView.Animation()
        self.companyLabel.text =  "\(self.viewModel.userModel[index].company?.name ?? .kEmpty), \(self.viewModel.userModel[index].company?.catchPhrase ?? .kEmpty), \(self.viewModel.userModel[index].company?.bs ?? .kEmpty)"
    }
    
    //MARK: - USER ASK THE LOGOUT
    @IBAction private func logutAction(){
        
        self.ShowAlert(.kLogout, Message.LogoutConfimation(), true) {
            UserDefaults.standard.removeObject(forKey: .kLOGIN_KEY)
            self.appDelegate.getLoginScreen()
        }
    }
}
