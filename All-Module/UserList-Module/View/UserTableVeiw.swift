//
//  UserTableVeiw.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation
import UIKit


//=============================
//MARK: - UITableViewDelegate
//=============================
extension UserVC : UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.userModel.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(
            withDuration: 0.5,
            delay: 0.05 * Double(indexPath.row),
            animations: {
                cell.alpha = 1
            })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.MoveToMapScreen(user: self.viewModel.userModel[indexPath.row])
    }
}

//===============================
//MARK: - UITableViewDataSource
//===============================
extension UserVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  =  tableView.dequeueReusableCell(withIdentifier: .kUserCell) as! UserCell
        cell.selectionStyle = .none
        cell.infoButton.tag = indexPath.row
        cell.infoButton.addTarget(self, action: #selector(showCompanyView(sender:)), for: .touchUpInside)
        cell.configureCell(user: self.viewModel.userModel[indexPath.row])
        return cell
    }
}
