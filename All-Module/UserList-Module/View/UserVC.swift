//
//  UserVC.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import UIKit

class UserVC: BaseVC {

    //MARK: - PROPETIES
    @IBOutlet weak var userTableView   : UITableView!
    @IBOutlet weak var addressView     : UIView!
    @IBOutlet weak var outerView       : UIView!
    @IBOutlet weak var headerLabel     : UILabel!
    @IBOutlet weak var companyLabel    : UILabel!
    @IBOutlet weak var logoutImageView : UIImageView!
    
    var viewModel = {
        UserVM()
    }()
    
    //MARK: - LOAD ALL VIEWS IN MEMORY 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.animateTheControl()
        self.registerCell()
        self.initViewModel()
    }
    
    //MARK: - REGISTER TABLEVIEW CELL
    private func registerCell(){
        self.userTableView.register(UINib(nibName: .kUserCell, bundle: nil), forCellReuseIdentifier: .kUserCell)
    }
    
    //MARK: - INIT VIEW MODEL CALL AND FETCH THE USERS LIST AND DISPLAY THE TABLE VIEW
    private func initViewModel(){
        self.viewModel.getUsersList()
        self.viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.userTableView.reloadData()
            }
        }
    }
}
