//
//  UserVM.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation


class UserVM  {
    
    //MARK: - PROPERTIES
    private var apiService: APIServiceDelegate
    var reloadTableView: (() -> Void)?
    
    init(apiService: APIServiceDelegate = APIServices(parseHandler: ParseHelper())) {
        self.apiService = apiService
    }
    var userModel = [UserCodable]() {
        didSet {
            reloadTableView?()
        }
    }
    
    //MARK: - GET USERS LIST
    public func getUsersList(){
        self.apiService.APICall(urlStr: baseURL, param: nil, mathod: .GET) { success, results, error  in
            if success, let result = results {
                self.fetchData(usersList: result)
            } else {
                print(error!)
            }
        }
    }
}

extension UserVM {
    //MARK: - FETCH USER LIST DATA
    func fetchData(usersList : [UserCodable]){
        self.userModel = usersList
    }
}
