//
//  AppDelegate.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let window =  UIWindow()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if UserDefaults.standard.bool(forKey: .kLOGIN_KEY){
            let isLogin = UserDefaults.standard.bool(forKey: .kLOGIN_KEY)
            switch isLogin {
            case true: self.getUsersListScreen()
                break
            case false: self.getLoginScreen()
                break
            }
        }else{
            self.getLoginScreen()
        }
        return true
    }
    
    //MARK: GET LOGIN SCREEN
    public func getLoginScreen(){
        let storyboard = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: IdentifireName.kLoginVC) as! ViewController
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden = true
        self.window.rootViewController = nav
        self.window.makeKeyAndVisible()
        
    }
    
    //MARK: GET USERS LIST SCFREEN
    public func getUsersListScreen(){
        let storyboard = UIStoryboard(name: Storyboard.Main.rawValue, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: IdentifireName.kUserVC) as! UserVC
        let nav = UINavigationController(rootViewController: vc)
        nav.isNavigationBarHidden = true
        self.window.rootViewController = nav
        self.window.makeKeyAndVisible()
    }
    
    
    //MARK: APPDELEGATE INSTANCE
    class func shared() -> AppDelegate {
        if Thread.isMainThread {
            return UIApplication.shared.delegate as! AppDelegate
        }
        let dg = DispatchGroup()
        dg.enter()
        var appDelegate: AppDelegate?
        DispatchQueue.main.async {
            appDelegate = UIApplication.shared.delegate as? AppDelegate
            dg.leave()
        }
        dg.wait()
        return appDelegate ?? UIApplication.shared.delegate as! AppDelegate
    }
}

