//
//  Enum.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation

enum Storyboard : String{
    case Main = "Main"
}

enum ImageName : String {
    case eyeOpen  = "ic-passwodShow"
    case eyeClose = "ic-passwodHide"
}
