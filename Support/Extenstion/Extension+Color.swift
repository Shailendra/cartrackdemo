//
//  Extension+Color.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation
import UIKit


extension UIColor {
    static func randomColor() -> UIColor {
        return UIColor(
            red:   .random(),
            green: .random(),
            blue:  .random(),
            alpha: 1.0
        )
    }
}
