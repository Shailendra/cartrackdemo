//
//  Extenstion.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//
import Foundation
import UIKit

extension String {
    
    static let kEmpty       = ""
    static let kOK          = "OK"
    static let kCredential  = "Credential"
    static let kCancel      = "Cancel"
    static let kYes         = "Yes"
    static let kLOGIN_KEY   = "LOGIN"
    static let kLogout      = "Logout"
    static let kUserCell    = "UserCell"
    static let kSqlite      = "userList.sqlite"
    static let kUsers       = "users"
    static let kCartrack    = "cartrack"
    static let kName        = "name"
    static let kPassword    = "password"
    static let kId          = "id"
    
    func trim() -> String {
       return self.trimmingCharacters(in: .whitespaces)
    }
}
