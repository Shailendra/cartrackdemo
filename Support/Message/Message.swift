//
//  Message.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation


class Message{
    
    class func EmptyUserNamePasswordCounty() -> String{
        return "Please enter username password and select county!"
    }
    
    class func EmptyUserName() -> String{
        return "Please enter username!"
    }
    
    class func EmptyPassword() -> String{
        return "Please enter password!"
    }
    
    class func EmptyCountry() -> String{
        return "Please select Country!"
    }
    
    class func NotMatchUserNamePassword() -> String{
        return "Username and password is not matched!"
    }
    
    class func LoginSuccssfully() -> String{
        return  "Login Succssfully!"
    }
    
    class func LogoutConfimation() -> String{
        return  "Are you sure you want to logout?"
    }
}
