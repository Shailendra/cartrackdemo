//
//  APIServices.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation


class APIServices : APIServiceDelegate {
   
    //MARK: - PROPETIES
    let parseHandler: ParseHelper
    
    //MARK: - DEGINATED INIT WITH PARAM
    init(parseHandler : ParseHelper){
        self.parseHandler = parseHandler
    }
    
    func APICall(urlStr: String, param: [String : Any]?,mathod : HttpMathod, completion: @escaping (Bool, [UserCodable]?, String?) -> ()) {
        
        let url = URL(string: urlStr )!
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("Error with fetching: \(error)")
                completion(false, nil, error.localizedDescription)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                print("Error with the response, unexpected status code: \(String(describing: response))")
                completion(false, nil, String(describing: response!))
                return
            }
            
            if let data = data {
                completion(true, self.parseHandler.parse(data: data), .kEmpty)
            }
        })
        task.resume()
    }
}

//MARK: - SOLID RULE
class ParseHelper{
    
    func parse(data: Data) -> [UserCodable] {
        let userList = try? JSONDecoder().decode([UserCodable].self, from: data)
        return userList ?? []
    }
}
