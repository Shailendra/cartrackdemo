//
//  HttpMathod.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation

enum HttpMathod : String {
    case GET     = "GET"
    case POST    = "POST"
    case PUT     = "PUT"
    case DELETE  = "DELETE"
    case HEAD    = "HEAD"
}
