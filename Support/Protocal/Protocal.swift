//
//  Protocal.swift
//  CartrackDemo
//
//  Created by Shailendra on 30/12/22.
//

import Foundation

protocol APIServiceDelegate {
    
    func APICall(urlStr: String, param: [String : Any]?,mathod : HttpMathod, completion: @escaping (Bool, [UserCodable]?, String?) -> ())
}

protocol DBDelegage{
    func insertValueInSQLite()
    func getUsersList()->Int
    func getUsersNameAndPassword()->(String,String)
    func getPath()->String
}

